package com.yalla.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Account {

	@Test
	public void account() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		
		driver.findElementById("email").sendKeys("jayanthiit3524@gmail.com");
		driver.findElementById("password").sendKeys("jacky@1993");
		driver.findElementById("buttonLogin").click();
		
		Thread.sleep(2000);
		
		Actions builder = new Actions(driver);
		WebElement eleVendors = driver.findElementByXPath("(//button[@type='button'])[6]");
		builder.moveToElement(eleVendors).perform();
		
		Thread.sleep(2000);
		driver.findElementByPartialLinkText("Search for Vendor").click();
		
		driver.findElementById("vendorTaxID").sendKeys("DE987564");
		driver.findElementById("buttonSearch").click();
		Thread.sleep(2000);
		
		String text = driver.findElementByXPath("//table[@class='table']/tbody/tr/following::tr/td").getText();
		System.out.println(text);
		
	}
	
}
