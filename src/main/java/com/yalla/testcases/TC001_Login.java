package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC001_Login extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_Login";
		testcaseDec = "Login into leaftaps";
		author = "Jayanthi V";
		category = "smoke";
		excelFileName = "TC001";
	} 
	
	@Test(dataProvider="fetchData")
	
	public void logIn(String uName, String pwd)
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin();
	}
}
