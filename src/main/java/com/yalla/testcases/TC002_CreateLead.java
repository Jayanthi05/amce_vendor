package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations {
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Creating a Lead";
		author = "Jayanthi V";
		category = "smoke";
		excelFileName = "CompName_FirstName_LastName";
	} 

	@Test(dataProvider="fetchData")
	public void createLead(String uName, String pwd, String cName, String FirstName, String LastName)
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompName(cName)
		.enterFirstName(FirstName)
		.enterLastName(LastName)
		.clickCreateLead()
		.verifyFirstName(FirstName);
	}
}
