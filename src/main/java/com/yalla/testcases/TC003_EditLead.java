package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{

	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Editing a Lead";
		author = "Jayanthi V";
		category = "smoke";
		excelFileName = "EditLead";
	} 

	@Test(dataProvider = "fetchData")
	public void editLead(String uName, String pwd, String fName, String updateCompName)
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(fName)
		.clickFindLeadsButton()
		.clickFirstResultingLead()
		.clickEdit()
		.updateCompanyName(updateCompName)
		.clickUpdate()
		.verifyCompName(updateCompName);
	}
	

}
