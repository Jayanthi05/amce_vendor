package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_MergeLead extends Annotations{

	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Editing a Lead";
		author = "Jayanthi V";
		category = "smoke";
		excelFileName = "TC001";
	} 
	
	@Test(dataProvider="fetchData")
	public void mergeLead(String uName, String pwd)
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLeads()
		.clickFromLead()
		.clickFromFirstResultingLead()
	/*	.clickToLead()
		.clickToResultingLead()
		.clickMergeLead()*/;
	}
}
