package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeafPage extends Annotations {
	
	public ViewLeafPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement eleVerifyFirstName;
	@FindBy(how= How.XPATH, using="(//a[@class='subMenuButton'])[3]") WebElement eleEditButton;
	@FindBy(how=How.ID, using="viewLead_companyName_sp") WebElement eleCompanyName;
	
	public ViewLeafPage verifyFirstName(String data)
	{
		verifyExactText(eleVerifyFirstName, data);
		return this;
			
	}
	
	public EditLeadPage clickEdit()
	{
		click(eleEditButton);
		return new EditLeadPage();
	}
	
	public ViewLeafPage verifyCompName(String data)
	{
		verifyPartialText(eleCompanyName, data);
		return this;
	}



}
