package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLeadPage extends Annotations{

	public EditLeadPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleUpdateCompName;
	@FindBy(how=How.XPATH, using="//input[@value='Update']") WebElement eleUpdate;
	
	public EditLeadPage updateCompanyName(String data)
	{
		clearAndType(eleUpdateCompName, data);
		return this;
	}
	
	public ViewLeafPage clickUpdate()
	{
		click(eleUpdate);
		return new ViewLeafPage();
	}
	
}
