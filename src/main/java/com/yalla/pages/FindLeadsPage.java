package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{
	
	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
	@FindBy(how = How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how = How.XPATH, using="(//a[@class='linktext'])[4]") WebElement eleFirstResultingLead;
	@FindBy(how = How.XPATH, using="(//a[@class='linktext'])[1]") WebElement eleFromFirstResultingLead;
	@FindBy(how = How.XPATH, using="(//a[@class='linktext'])[6]") WebElement eleToResultingLead;
	
	
	public FindLeadsPage enterFirstName(String data)
	{
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public FindLeadsPage clickFindLeadsButton()
	{
		click(eleFindLeads);
		return this;
	}
	
	public ViewLeafPage clickFirstResultingLead()
	{
		click(eleFirstResultingLead);
		return new ViewLeafPage();
	}
	
	public MergeLeadsPage clickFromFirstResultingLead()
	{
		click(eleFromFirstResultingLead);
		//switchToWindow(0);
		return new MergeLeadsPage();
	}
	
	public MergeLeadsPage clickToResultingLead()
	{
		click(eleToResultingLead);
		//switchToWindow(0);
		return new MergeLeadsPage();
	}

	

}
