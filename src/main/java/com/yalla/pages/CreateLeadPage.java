package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {

	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID , using = "createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID, using ="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.XPATH, using="//input[@name='submitButton']") WebElement eleClickCreateLead;
	
	public CreateLeadPage enterCompName(String data)
	{
		clearAndType(eleCompanyName, data);
		return this;
	}

	public CreateLeadPage enterFirstName(String data)
	{
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public CreateLeadPage enterLastName(String data)
	{
		clearAndType(eleLastName, data);
		return this;
	}
	
	public ViewLeafPage clickCreateLead()
	{
		click(eleClickCreateLead);
		return new ViewLeafPage();
	}
	
	
}
