package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadsPage extends Annotations{
	
	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//input[@id='partyIdFrom']/following::a/img)[1]") WebElement eleFromLead;
	@FindBy(how=How.XPATH, using="(//input[@id='partyIdFrom']/following::a/img)[2]") WebElement eleToLead;
	@FindBy(how=How.XPATH, using="//a[@class='buttonDangerous']") WebElement eleMergeLead;
	
	public FindLeadsPage clickFromLead()
	{
		click(eleFromLead);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	public FindLeadsPage clickToLead()
	{
		click(eleToLead);
		//switchToWindow(2);
		return new FindLeadsPage();
	}
	
	public ViewLeafPage clickMergeLead()
	{
		click(eleMergeLead);
		acceptAlert();
		return new ViewLeafPage();
	}


}
